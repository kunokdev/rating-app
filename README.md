# rating-app

Sample aplication for rating food items

## Requirements

- Docker

## Running

### Production

#### Using docker-compose

Using docker-compose you can simply run `docker-compose up` and visit http://localhost then add proxy (e.g. https nginx) to expose it to the internet.

#### Using vanilla Docker

You can build image by running `docker build . -t rating-app`, and then you can run container via `docker run -p 80:80 -t rating-app` and visit http://localhost within your browser.

### Development

Simply run `yarn dev` and project will be automatically open in your browser on the first available port.

## Configuration

This project supports **runtime configuration** which means that you can change environment variable (within docker-compose or via docker flags) and you will be able to reconfigure `INTERVAL` without rebuilding images.

Basically `INTERVAL` variable defines how often (in miliseconds) will items by randomly rated. You may change default value in `.env` file, but do not use it for runtime configuration. See `docker-compose.yml` file to see example how to reconfigure it in runtime.

## Testing

Use `yarn test` to run tests

## License

UNLICENSED; Anything within this repository may be used for any type of purpose, both commerical or educational.
