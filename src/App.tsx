import React from "react";
import { Provider } from "react-redux";

import store from "redux/store";
import { RouterContainer, Routes } from "containers/Router";
import BaseLayout from "components/BaseLayout";

class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <RouterContainer>
          <BaseLayout>
            <Routes />
          </BaseLayout>
        </RouterContainer>
      </Provider>
    );
  }
}

export default App;
