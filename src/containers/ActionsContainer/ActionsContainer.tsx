import React from "react";

import Actions from "components/Actions";
import { connect } from "react-redux";
import randomRating from "instances/randomRating";
import { selectIsRunning } from "redux/modules/items";
import { StoreType } from "redux/types";

interface PropTypes {
  running: boolean;
}

class ActionsContainer extends React.Component<PropTypes> {
  render() {
    const { running } = this.props;
    return (
      <Actions
        onClick={randomRating.toggleRating}
        icon={running ? "stop" : "play"}
        text={running ? "Stop running" : "Start running"}
      />
    );
  }
}

export default connect(
  (state: StoreType) => ({ running: selectIsRunning(state)() }),
  null
)(ActionsContainer);
