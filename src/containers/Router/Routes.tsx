// @flow
import * as React from "react";
import { Switch, Route } from "react-router-dom";

import Page404 from "views/Page404";
import Root from "views/Root";

type PropTypes = {};

class Routes extends React.Component<PropTypes> {
  render() {
    return (
      <Switch>
        <Route exact path="/" component={Root} />
        <Route component={Page404} />
      </Switch>
    );
  }
}

export default Routes;
