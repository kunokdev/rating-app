import React from "react";
import { connect } from "react-redux";
import { Container } from "semantic-ui-react";
import { Flipped, Flipper } from "react-flip-toolkit";

import RatableItem from "components/RatableItem";
import { IItem, selectItems, loadItems, vote } from "redux/modules/items";
import { StoreType } from "redux/types";

interface PropTypes {
  items: IItem[];
  loadItems: Function;
  vote: (type: string, id: string) => any;
}

class ItemsContainer extends React.Component<PropTypes> {
  componentDidMount() {
    this.props.loadItems();
  }

  render() {
    const flipKey = this.props.items.map(({ id }) => id).join("");
    return (
      <Container>
        <Flipper flipKey={flipKey}>
          {this.props.items.map(e => (
            <Flipped key={e.id} flipId={e.id}>
              <div>
                <RatableItem onClick={this.props.vote} {...e} />
              </div>
            </Flipped>
          ))}
        </Flipper>
      </Container>
    );
  }
}

export default connect(
  (state: StoreType) => ({
    items: selectItems(state)()
  }),
  {
    loadItems,
    vote
  }
)(ItemsContainer);
