import store from "redux/store";
import { vote, selectItems, toggleRunning } from "redux/modules/items";
import randomNumber from "utils/randomNumber";
import { ExtendedWindow } from "../types";

const extendedWindow: ExtendedWindow = window;
const configuredInterval = extendedWindow && extendedWindow._env_ && extendedWindow._env_.INTERVAL;

class RandomRating {
  ids: string[];
  intervalInstance: any;
  constructor() {
    this.ids = [];
    this.intervalInstance = null;
  }

  rate = () => {
    const type = randomNumber(0, 1) ? "negative" : "positive";
    const id = this.ids[randomNumber(0, this.ids.length - 1)];
    if (!id) return;
    store.dispatch(vote(type, id));
  };

  toggleRating = () => {
    store.dispatch(toggleRunning());
    if (this.intervalInstance) {
      clearInterval(this.intervalInstance);
      this.intervalInstance = null;
      return;
    }
    this.ids = selectItems(store.getState())().map(({ id }) => id);
    this.intervalInstance = setInterval(() => this.rate(), Number(configuredInterval) || 250);
  };
}

export default new RandomRating();
