export const resolveOrExpressionChain = (...expressions: any): any => {
  for (const expression of expressions) if (expression || expression === 0) return expression;
  return expressions[expressions.length - 1];
};
