import React from "react";
import { create, act } from "react-test-renderer";
import store from "redux/store";
import { Provider } from "react-redux";

import ActionsContainer from "containers/ActionsContainer";

describe("Actions container", () => {
  test("When actions container button is clicked change its text", () => {
    const component = create(
      <Provider store={store}>
        <ActionsContainer />
      </Provider>
    );
    const instance = component.root.findByType("button");
    expect(instance.children[instance.children.length - 1]).toBe("Start running");
    act(() => instance.props.onClick());
    expect(instance.children[instance.children.length - 1]).toBe("Stop running");
  });
});
