import React from "react";
import { Container } from "semantic-ui-react";

import ItemsContainer from "containers/ItemsContainer";
import styles from "./styles.module.scss";

export default class Root extends React.Component {
  render() {
    return (
      <Container className={styles.container}>
        <ItemsContainer />
      </Container>
    );
  }
}
