import React from "react";
import { Icon, Container, Grid } from "semantic-ui-react";

import ActionsContainer from "containers/ActionsContainer";
import styles from "./styles.module.scss";

interface PropTypes {
  children: any;
}

const Header = () => (
  <header className={styles.header}>
    <Container>
      <Grid columns="equal">
        <Grid.Column>
          <h1>
            <Icon name="food" />
            <span>Food Rating</span>
          </h1>
        </Grid.Column>
        <Grid.Column textAlign="right">
          <ActionsContainer />
        </Grid.Column>
      </Grid>
    </Container>
  </header>
);

const BaseLayout = ({ children }: PropTypes) => (
  <div className={styles.container}>
    <Header />
    <div>{children}</div>
  </div>
);

export default BaseLayout;
