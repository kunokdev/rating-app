import React, { MouseEvent } from "react";
import { Button, Icon } from "semantic-ui-react";

import styles from "./styles.module.scss";

interface PropTypes {
  onClick: (event: MouseEvent<HTMLButtonElement>) => any;
  text: string;
  icon: "play" | "stop";
}

const Actions = ({ onClick, text, icon }: PropTypes) => (
  <div className={styles.container}>
    <Button primary onClick={onClick}>
      <Icon name={icon} />
      {text}
    </Button>
  </div>
);

export default Actions;
