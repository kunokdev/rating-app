import React from "react";
import { Button, Image, Grid, Header, Divider } from "semantic-ui-react";
import { POSITIVE, NEGATIVE } from "redux/modules/items";

import styles from "./styles.module.scss";

interface PropTypes {
  id: string;
  image: string;
  name: string;
  positive: number;
  negative: number;
  onClick: (type: string, id: string) => any;
}

export default class ItemComponent extends React.Component<PropTypes> {
  render() {
    const { id, image, name, positive, negative, onClick } = this.props;
    return (
      <div className={styles.container}>
        <Grid>
          <Grid.Column width={4}>
            <Image src={image} />
          </Grid.Column>
          <Grid.Column width={12}>
            <Header>{name.substring(0, 1).toUpperCase() + name.substring(1, name.length)}</Header>
            <p>
              {(() => {
                if (positive - negative === 0) return "I am not sure... 🤔";
                if (positive - negative > 0) return "You gotta love this delicious meal! 😋";
                return "Something is wrong with this meal! 🤮";
              })()}
            </p>
            <Button.Group width={3}>
              <Button onClick={() => onClick(POSITIVE, id)} size="big">
                <span role="img" aria-label="Thumbs up">
                  👍
                </span>{" "}
                {positive}
              </Button>
              <Button onClick={() => onClick(NEGATIVE, id)} size="big">
                <span role="img" aria-label="Thumbs down">
                  👎
                </span>{" "}
                {negative}
              </Button>
            </Button.Group>
          </Grid.Column>
        </Grid>
        <Divider />
      </div>
    );
  }
}
