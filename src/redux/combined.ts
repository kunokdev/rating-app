import { combineReducers } from "redux";
import { reducer as items } from "./modules/items";

export default combineReducers({
  items
});
