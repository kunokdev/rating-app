import { createStore, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import thunkMiddleware from "redux-thunk";
import logger from "redux-logger";

import combinedReducers from "./combined";

const store = createStore(
  combinedReducers,
  composeWithDevTools(
    applyMiddleware(thunkMiddleware),
    ...(process.env.NODE_ENV === "development" ? [applyMiddleware(logger)] : [])
  )
);

export default store;
