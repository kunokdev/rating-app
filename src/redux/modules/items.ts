import { ExtendedActionType, StoreType } from "redux/types";
import food from "../../assets/food.json";
import objectifyArray from "utils/objectifyArray";
import mapToArray from "utils/mapToArray";
import randomNumber from "utils/randomNumber";

export const ITEMS = "items";
const SET = `${ITEMS}/SET`;
const VOTE = `${ITEMS}/VOTE`;
const TOGGLE_RUNNING = `${ITEMS}/TOGGLE_RUNNING`;
export const POSITIVE = "positive";
export const NEGATIVE = "negative";

export interface IItem {
  id: string;
  name: string;
  image: string;
  positive: number;
  negative: number;
}

export interface ItemsState {
  items: IItem[];
  order: string[];
  running: boolean;
}

const initialState: ItemsState = {
  items: [],
  order: [],
  running: false
};

// Action creators
export function loadItems() {
  const items = createFoodObjectsHashTable(food);
  return {
    type: SET,
    payload: items
  };
}

export function vote(type: string, id: string) {
  return {
    type: VOTE,
    payload: {
      id,
      type
    }
  };
}

export function toggleRunning() {
  return {
    type: TOGGLE_RUNNING
  };
}

// Utils
function createFoodObjectsHashTable(food: string[]) {
  return objectifyArray(
    food.map(
      (name, id): IItem => ({
        id: `${id}-${name}`,
        name,
        image: `/images/${name}.jpg`,
        positive: randomNumber(0, 10),
        negative: randomNumber(0, 10)
      })
    )
  );
}

function resolveScore(positive: number, negative: number) {
  return positive - negative;
}

function sortByVote(items: IItem[]) {
  return items.sort((a, b) =>
    resolveScore(a.positive, a.negative) < resolveScore(b.positive, b.negative) ? 1 : -1
  );
}

// Selectors
export const selectItems = (state: StoreType) => () => sortByVote(mapToArray(state[ITEMS].items));
export const selectIsRunning = (state: StoreType) => () => state[ITEMS].running;

export function reducer(state = initialState, action: ExtendedActionType) {
  switch (action.type) {
    case SET:
      return {
        ...state,
        items: action.payload[0],
        order: action.payload[1]
      };
    case VOTE:
      return {
        ...state,
        items: {
          ...state.items,
          [action.payload.id]: {
            ...state.items[action.payload.id],
            ...(action.payload.type === POSITIVE
              ? { positive: 1 + state.items[action.payload.id].positive }
              : { negative: 1 + state.items[action.payload.id].negative })
          }
        }
      };
    case TOGGLE_RUNNING:
      return {
        ...state,
        running: !state.running
      };
    default:
      return state;
  }
}
