import { ItemsState } from "./modules/items";

export type ActionType = {
  type: string;
  payload: any;
};

export type ExtendedActionType = {
  type: string;
  payload: any;
  meta: {};
};

export type StoreType = {
  [key: string]: any;
  items: ItemsState;
};
